/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1861948
 */
public class PontoXY extends Ponto2D{
    
    public PontoXY () {
        super();
    }
    
    public PontoXY(double x, double y) {
        super(x, y, 0);
    }
    
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    @Override
    public String toString() {
        return String.format(getNome()+"("+getX()+","+getY()+")");
    }
}
